import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:29:01.534458
#2018-02-27 09:46:02.117662
#2018-02-27 10:01:01.944961
#2018-02-27 10:27:01.427800
#2018-02-27 10:52:02.315179
#2018-02-27 11:46:01.303083
#2018-02-27 13:01:02.212756
#2018-02-27 13:22:01.609974
#2018-02-27 13:44:01.298319
#2018-02-27 14:02:01.501999
#2018-02-27 14:16:01.268138
#2018-02-27 14:38:01.266414
#2018-02-27 15:04:01.500059
#2018-02-27 15:27:01.352029
#2018-02-27 15:44:01.669336
#2018-02-27 16:15:01.382659
#2018-02-27 17:01:02.121594
#2018-02-27 17:46:01.600094
#2018-02-27 18:49:01.366428
#2018-02-27 19:34:02.187374
#2018-02-27 20:38:01.220663